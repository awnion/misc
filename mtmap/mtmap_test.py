import time
from mtmap import mtmap
from itertools import imap
import urllib

def fibonacci():
    a = 0
    b = 1
    while True:
        c = a + b
        a, b = b, c
        yield a

def test_random():
    import random
    i = 0
    while True:
        i += 1
        if random.randint(0, 1000) != 5:
            yield i
        else:
            raise Exception('test')

def test_length():
    for i in xrange(10):
        yield i

if __name__ == "__main__":
#    it = mtmap(lambda x: -x, fibonacci(), 30)
#    for i in range(50):
#        print it.next()
    it = mtmap(lambda x: -x, test_length(), 2)
    for i in xrange(11):
        print it.next()
#
#    it = imap(lambda x: -x, test_length())
#    for i in xrange(11):
#        print it.next()
#    def f(post):
#        return urllib.urlopen('http://stackoverflow.com/questions/%u' % post)
#
#    start = time.time()
#    it = mtmap(f, xrange(3329361, 3329361 + 50), 50)
#    for i in it:
#        pass
#    print time.time() - start
#
#    start = time.time()
#    it = mtmap(f, xrange(3329361, 3329361 + 50), 10)
#    for i in it:
#        pass
#    print time.time() - start
#
#    start = time.time()
#    it = imap(f, xrange(3329361, 3329361 + 50))
#    for i in it:
#        pass
#    print time.time() - start

