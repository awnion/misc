import sys
import Queue
import threading


class WorkerThread(threading.Thread):
    """
    A worker thread sits in the background and picks up work requests from
    one queue and puts the results in another until it is dismissed.
    """

    def __init__(self, callable, requests_queue, results_queue,
                 poll_timeout=3, **kwds):
        """
        Set up thread in daemonic mode and start it immediatedly.
        """
        threading.Thread.__init__(self, **kwds)
        self.setDaemon(True)
        self._callable = callable
        self._requests_queue = requests_queue
        self._results_queue = results_queue
        self._poll_timeout = poll_timeout
        self._dismissed = threading.Event()
        self.start()

    def run(self):
        """
        Repeatedly process the job queue until told to exit.
        """
        while True:
            if self._dismissed.isSet():
                # we are dismissed, break out of loop
                break

            # get next work request. If we don't get a new request from the
            # queue after self._poll_timout seconds, we jump to the start of
            # the while loop again, to give the thread a chance to exit.
            try:
                request = self._requests_queue.get(True, self._poll_timeout)
            except Queue.Empty:
                continue
            else:
                try:
                    if self._callable is None:
                        result = tuple(request)
                    else:
                        result = self._callable(*request)

                    # if first in result tuple is True, we have no exceptions
                    self._results_queue.put((True, result))
                except:
                    request.exception = True
                    self._results_queue.put((False, sys.exc_info()))

    def dismiss(self):
        """
        Sets a flag to tell the thread to exit when done with current job.
        """
        self._dismissed.set()


def init_workers(klass, size, *args):
    """
    Create pool of size workers and return as list.
    """
    return [klass(*args) for _ in xrange(size)]


def stop_workers(workers):
    """
    Dismiss and join workers to main thread
    """
    for w in workers:
        w.dismiss()
    for w in workers:
        w.join()

def mtmap(function, *args):
    """
    Asynchronous multithreaded analogue of itertools.imap

    Basic usage:
        >>> iter = mtmap(pow, xrange(10), xrange(10), 3)
        >>> map(None, iter)
        [1, 1, 4, 27, 256, 3125, 46656, 823543, 16777216, 387420489]
        >>> iter.next()
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        StopIteration
    """
    if len(args) < 2:
        raise TypeError('mtmap() must have at least three arguments.')

    # get number of threads from args and check it
    k = args[-1]
    if not isinstance(k, int) or k < 1:
        raise ValueError('Number of threads must be exact positive integer.')

    # init iterables from args
    iterables = map(iter, args[:-1])

    # init queues for requests and results
    requests_queue = Queue.Queue()
    results_queue = Queue.Queue()

    # init pool of threads
    workers = init_workers(WorkerThread, k, function,
                           requests_queue, results_queue)

    # distribute initial k tasks or less if k > length of iterables
    in_work = 0
    try:
        while in_work < k:
            args = [next(it) for it in iterables]
            requests_queue.put(args)
            in_work += 1
    except StopIteration:
        pass

    # generate responses so far have requests
    while in_work > 0:
        # get result of one task
        result = results_queue.get(True)
        in_work -= 1

        # check for exception
        if not result[0]:
            exception = result[1][:2]

            stop_workers(workers)

            raise exception[0], exception[1], exception[2]

        # create new task in place of the old one
        try:
            args = [next(it) for it in iterables]
            requests_queue.put(args)
            in_work += 1
        except StopIteration:
            pass

        yield result[1]

    stop_workers(workers)
