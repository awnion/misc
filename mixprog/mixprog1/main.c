#include "mixprog.h"

int main (int argc, char const* argv[]) {
    mixprog_test("test_bool1.txt", "result_bool1.txt");
    mixprog_test("test_bool2.txt", "result_bool2.txt");

    mixprog_test("test_1.txt", "result_1.txt");
    mixprog_test("test_2.txt", "result_2.txt");
    mixprog_test("test_3.txt", "result_3.txt");
    mixprog_test("test_4.txt", "result_4.txt");
    mixprog_test("test_5.txt", "result_5.txt");
    mixprog_test("test_6.txt", "result_6.txt");
    mixprog_test("test_7.txt", "result_7.txt");
    mixprog_test("test_8.txt", "result_8.txt");
    mixprog_test("test_9.txt", "result_9.txt");
    mixprog_test("test_10.txt", "result_10.txt");
    mixprog_test("test_11.txt", "result_11.txt");
    mixprog_test("test_12.txt", "result_12.txt");
    mixprog_test("test_13.txt", "result_13.txt");
    mixprog_test("test_14.txt", "result_14.txt");
    mixprog_test("test_15.txt", "result_15.txt");
   
    return 0;
}
