#include "error.h"

int error_report (const char message[]) {
    printf("%s\n", message);
    return 0;
}

int error_file_not_exists (const char filename[]) {
    printf("Error: file '%s' not exists.\n", filename);
    return 0;
}
