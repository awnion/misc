#ifndef _MIXPROG_H_
#define _MIXPROG_H_

#include <stdio.h>

#include "simplex.h"
#include "linalg.h"

static int             GLOBAL_ID           = 0;

typedef struct {
    matrix          A;              // матрица А
    vector          B;              // правые части ограничений
    vector          C;              // коэффициенты целевой функции

    int             id;
    
    int             has_solution;   // имеет ли решение система 
    vector          solution_x;     // вектор решения - X
    vector          solution_y;     // вектор решения - Y
    float           solution_f;     // значение целевой функции в решении
                                    // она же "Оценка" $\mu^0$

    int             k;              // кол-во ограничений
    int             n;              // кол-во переменных x
    int             m;              // кол-во переменных y

    vector          type;           // тип уравнения ограничений: 
                                    // 1 -> "<=", 0 -> "==", -1 -> ">="
} mixprog_system;

struct tmixprog_list {
    mixprog_system          *system;

    struct tmixprog_list    *next;
};
typedef struct tmixprog_list mixprog_list;

int mixprog_is_not_boolean (mixprog_system *system);
int mixprog_list_add (mixprog_list **list_end, mixprog_system *system);
int mixprog_list_del (mixprog_list *prev_elem, mixprog_list **list_end);
int mixprog_boolean_split (mixprog_system *system, mixprog_list **list, int number, FILE *file);

int mixprog_test (char *input_filename, char *output_filename);

int mixprog_solve (mixprog_system *system);
int mixprog_fread (FILE *file, mixprog_system *system);
int mixprog_fwrite (FILE *file, mixprog_system *system);
int mixprog_fwrite_solution (FILE *file, mixprog_system *system);
int mixprog_fwrite_line (FILE *file);
int mixprog_new (mixprog_system **system, int k, int n, int m);
int mixprog_copy (mixprog_system *src, mixprog_system *rsv);
int mixprog_free (mixprog_system **system);

#endif /* _MIXPROG_H_ */
