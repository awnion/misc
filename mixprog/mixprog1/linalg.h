#ifndef _LINALG_H_
#define _LINALG_H_

#include <stdio.h>
#include <stdlib.h>

typedef float **matrix;
typedef float *vector;

int linalg_new_matrix (matrix *m, int height, int width);
int linalg_del_matrix (matrix *m, int height);
int linalg_new_vector (vector *v, int width);
int linalg_del_vector (vector *v);
int linalg_fread_matrix (FILE *file, matrix m, int height, int width);
int linalg_fread_vector (FILE *file, vector v, int width);
int linalg_fwrite_matrix (FILE *file, matrix m, int height, int width);
int linalg_fwrite_vector (FILE *file, vector v, int width);
int linalg_invert_vector (vector v, int width);

#endif /* _LINALG_H_ */
