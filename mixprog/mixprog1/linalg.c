#include "linalg.h"

int linalg_new_matrix (matrix *m, int height, int width) {
    int i;
    
    // TODO: обработка ошибок выделения памяти
    
    *m = (matrix) calloc(height, sizeof(vector));
    if (*m == NULL) {
        exit(1);
    }
    
    for (i = 0; i < height; i += 1) {
        linalg_new_vector(&((*m)[i]), width); // вот такая вот ссылка на вектор внутри матрицы
    }
    
    return 0;
}

int linalg_del_matrix (matrix *m, int height) {
    int i;
    
    for (i = 0; i < height; i += 1) {
        linalg_del_vector(&((*m)[i]));
    }
    free(*m);
    *m = NULL;
    
    return 0;
}

int linalg_new_vector (vector *v, int width) {
    int i;
    
    // TODO: обработка ошибок выделения памяти
    
    *v = (vector) calloc(width, sizeof(double));
    if (*v == NULL) {
        exit(1);
    }
    
    // Заполняем нулями
    for (i = 0; i < width; i += 1) {
        (*v)[i] = 0;
    }
    
    return 0;
}

int linalg_del_vector (vector *v) {
    free(*v);
    *v = NULL;

    return 0;
}

int linalg_fread_matrix (FILE *file, matrix m, int height, int width) {
    int i;

    for (i = 0; i < height; i += 1) {
        linalg_fread_vector(file, m[i], width);
    }

    return 0;
}

int linalg_fread_vector (FILE *file, vector v, int width) {
    int		i;
    float	f;

    for (i = 0; i < width; i += 1) {
        fscanf(file, "%f", &v[i]);
    }

    return 0;
}

int linalg_fwrite_matrix (FILE *file, matrix m, int height, int width) {
    int i;

    for (i = 0; i < height; i += 1) {
        linalg_fwrite_vector(file, m[i], width);
    }

    return 0;
}

int linalg_fwrite_vector (FILE *file, vector v, int width) {
    int i;

    for (i = 0; i < width; i += 1) {
        fprintf(file, "%6.2f ", v[i]);
    }
    fprintf(file, "\n");

    return 0;
}

// умножение вектора на -1
int linalg_invert_vector (vector v, int width) {
    int i;

    for (i = 0; i < width; i += 1) {
        v[i] *= -1;
    }

    return 0;
}
