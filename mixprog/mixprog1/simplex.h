#ifndef _SIMPLEX_H_
#define _SIMPLEX_H_

#include <stdlib.h>
#include <stdio.h>
#include "linalg.h"

#define INFTY 1000000

typedef struct {
    matrix          A;              // матрица А
    vector          B;              // правые части ограничений
    vector          C;              // коэффициенты целевой функции
    
    int             has_solution;   // имеет ли решение система 
    vector          solution_x;     // вектор решения - X
    float           solution_f;     // значение целевой функции в решении

    int             n;              // кол-во переменных
    int             m;              // кол-во ограничений
    vector          type;           // тип уравнения ограничений: 1 -> "<=", 0 -> "==", -1 -> ">="
} simplex_system;

int simplex_create_table (simplex_system *system, matrix *table, int *basis);
int simplex_solve (simplex_system *system);
int simplex_test (char *input_filename, char *output_filename);

#endif /* _SIMPLEX_H_ */
