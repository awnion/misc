#include "mixprog.h"
#include "error.h"

// выдает номер не булевого элемента + 1 или 0 - если все булевые
int mixprog_is_not_boolean (mixprog_system *system) {
    int         i;

    for (i = 0; i < system->m; i += 1) {
        if (system->solution_y[i] != 1 && system->solution_y[i] != 0) {
            return i + 1;
        }
    }

    return 0;
}

int mixprog_list_add (mixprog_list **list_end, mixprog_system *system) {
    mixprog_list        *tmp;

    // выделяем память под новый элемент
    tmp = (mixprog_list *) malloc(sizeof(mixprog_list));

    tmp->next = NULL;
    tmp->system = system;

    (*list_end)->next = tmp;
    (*list_end) = tmp;

    return 0;
}

int mixprog_list_del (mixprog_list *prev_elem, mixprog_list **list_end) {
    mixprog_list        *tmp;

    tmp = prev_elem->next->next;

    mixprog_free(&prev_elem->next->system); 
    free(prev_elem->next);

    prev_elem->next = tmp;

    if (prev_elem->next == NULL) {
        *list_end = prev_elem;
    }

    return 0;
}

// берёт систему и номер элемента, генерирует 2 новых задачи, решает их
// и записывает их в список (удалять старое решение надо, если надо, отдельно)
// что полезно: выписывает порожденные решения в файл, если ссылка на файл не NULL
// не оптимальные решения даже не вписывает
int mixprog_boolean_split (mixprog_system *system, mixprog_list **list, int number, FILE *file) {
    int                 i;
    int                 j;

    mixprog_system      *s1;
    mixprog_system      *s2;

    // создаем ровые системы
    mixprog_new(&s1, system->k + 1, system->n, system->m);
    mixprog_new(&s2, system->k + 1, system->n, system->m);
    
    // копируем
    mixprog_copy(system, s1);
    mixprog_copy(system, s2);

    // дописываем ограничения
    s1->A[s1->k - 1][s1->n + number] = 1;
    s1->B[s2->k - 1] = 1;
    s1->type[s2->k - 1] = 0; // равенство

    s2->A[s2->k - 1][s2->n + number] = 1;
    s2->B[s2->k - 1] = 0;
    s2->type[s2->k - 1] = 0; // равенство

    // решаем
    mixprog_solve(s1);
    mixprog_solve(s2);

    // запивыем в файл
    if (file != NULL) {
        mixprog_fwrite_line(file);
        fprintf(file, "Добавляем новую задачу: %7d\n", s1->id);
        mixprog_fwrite_line(file);

        mixprog_fwrite(file, s1);

        mixprog_fwrite_line(file);
        fprintf(file, "Добавляем новую задачу: %7d\n", s2->id);
        mixprog_fwrite_line(file);

        mixprog_fwrite(file, s2);
    }

    // довавляем в конец списка
    mixprog_list_add(list, s1);
    mixprog_list_add(list, s2);

    return 0;
}

int mixprog_test (char *input_filename, char *output_filename) {
    FILE                *input;
    FILE                *output;

    mixprog_system      *main_system;

    int                 is_not_boolean;
    int                 i;

    float               tmp;

    mixprog_list        *list;
    mixprog_list        *list_end;

    mixprog_list        *lcur; // текущее решение
    mixprog_list        *ltmp; // "бегунок"
    mixprog_list        *lmax; // буфер максимума

    input = fopen(input_filename, "r");
    output = fopen(output_filename, "w");

    GLOBAL_ID = 0;

    // TODO: номера задач

    if (input != NULL && output != NULL) {
        main_system = (mixprog_system *) malloc(sizeof(mixprog_system));

        GLOBAL_ID += 1;
        main_system->id = GLOBAL_ID;

        mixprog_fread(input, main_system);

        if (main_system->k <= 0) {
            main_system->has_solution = -4;
        } else {
            mixprog_solve(main_system);
        }

        if (main_system->has_solution != 1) {
            switch (main_system->has_solution) {
                case -1:
                    fprintf(output, "Решения нет.\nСистема не ограничена.\n");
                    break;
                case -2:
                    fprintf(output, "Решения нет.\nОграничения не совместны.\n");
                    break;
                case -4:
                    fprintf(output, "Решения нет.\nСистема не имеет ограничений.\n");
                    break;
                default:
                    // сюда по-идее мы не должны попасть
                    fprintf(output, "Ошибка.\nНеверный формат данных или системный сбой.\n");
                    break;
            }
        } else {
            is_not_boolean = mixprog_is_not_boolean(main_system);

            mixprog_fwrite_line(output);
            fprintf(output, "Начальная задача:\n");
            mixprog_fwrite_line(output);

            mixprog_fwrite(output, main_system);

            // если решение не булевое
            if (is_not_boolean) {
                // создаём список
                // первый элемент списка - фиктивный, это для удобства работы
                list = (mixprog_list *) malloc(sizeof(mixprog_list));
                list->next = NULL;
                list->system = NULL;
                list_end = list;

                // делаем нывае задачи
                mixprog_boolean_split(main_system, &list_end, is_not_boolean - 1, output);

                // текущее решение не определено
                lcur = NULL;

                //////////////////////////////////////
                // делаем цикл, который всё решает
                // пока в списке > 1 элемента
                while (list->next != NULL && list->next->next != NULL) {
                    // ищем задачу с максимальной оценкой
                    lmax = list;
                    ltmp = list;
                    while (ltmp->next != NULL) {
                        if (ltmp->next->system->has_solution == 1 && ltmp->next->system->solution_f > lmax->next->system->solution_f) {
                            lmax = ltmp;
                        }
                        ltmp = ltmp->next;
                    }


                    // если ни у одной задачи нет решения, то удаляем все задачи и выходим
                    if (lmax->next->system->has_solution != 1) {
                        ltmp = list;
                        while (ltmp->next != NULL) {
                            mixprog_fwrite_line(output);
                            fprintf(output, "Удаляем задачу: %7d\n", ltmp->next->system->id);
                            mixprog_fwrite_line(output);

                            mixprog_list_del(ltmp, &list_end);
                        }
                        break;
                    }

                    // проверяем "булевость"
                    is_not_boolean = mixprog_is_not_boolean(lmax->next->system);

                    if (is_not_boolean) {
                        // делаем нывае задачи
                        mixprog_boolean_split(lmax->next->system, &list_end, is_not_boolean - 1, output);

                        mixprog_fwrite_line(output);
                        fprintf(output, "Удаляем задачу: %7d\n", lmax->next->system->id);
                        mixprog_fwrite_line(output);

                        // удаляем старую
                        mixprog_list_del(lmax, &list_end);
                    } else {
                        lcur = lmax->next;
                        ltmp = list;
                        while (ltmp->next != NULL) {
                            // либо удалили, либо сдвинулись
                            if ((ltmp->next != lcur && ltmp->next->system->solution_f <= lcur->system->solution_f) || ltmp->next->system->has_solution != 1) {
                                mixprog_fwrite_line(output);
                                fprintf(output, "Удаляем задачу: %7d\n", ltmp->next->system->id);
                                mixprog_fwrite_line(output);

                                mixprog_list_del(ltmp, &list_end);
                            } else {
                                ltmp = ltmp->next;
                            }
                        }
                    }
                }

                fprintf(output, "\n\n");

                if (list->next == NULL) {
                    mixprog_fwrite_line(output);
                    fprintf(output, "Решения НЕТ\n");
                    mixprog_fwrite_line(output);
                } else {
                    mixprog_fwrite_line(output);
                    fprintf(output, "Решение:\n");
                    mixprog_fwrite_line(output);

                    mixprog_fwrite_solution(output, list->next->system);
                    mixprog_list_del(list, &list_end);
                }
                free(list);
            } else {
                fprintf(output, "\n\n");

                mixprog_fwrite_line(output);
                fprintf(output, "Решение:\n");
                mixprog_fwrite_line(output);

                mixprog_fwrite_solution(output, main_system);
            }
        }

        // убираем мусор 
        mixprog_free(&main_system);

        fclose(input);
        fclose(output);
    } else {
        if (input == NULL) {
            error_file_not_exists(input_filename);
        }
        if (output == NULL) {
            error_file_not_exists(output_filename);
        }
    }
    

    return 0;
}

int mixprog_solve (mixprog_system *system) {
    simplex_system      s;

    s.m         = system->k;
    s.n         = system->n + system->m;

    // копируем ссылки на векторы
    s.C         = system->C;
    s.A         = system->A;
    s.B         = system->B;
    s.type      = system->type;

    // DO IT :)
    simplex_solve(&s);

    system->has_solution = s.has_solution;

    if (s.has_solution == 1) {
        system->solution_f = s.solution_f;
        // тут память освобождать не надо, т.к. мы передаем 
        // указатели из временной системы в "постоянную"
        system->solution_x = s.solution_x;
        // хинт, ссылка на тот же массив, но сразу с n+1 эл-та
        // т.е. туда, где начинается Y
        system->solution_y = s.solution_x + system->n;
    }

    return 0;
}

int mixprog_fread (FILE *file, mixprog_system *system) {
    int         i;
    int         k;
    int         n;
    int         m;

    if (fscanf(file, "%d %d %d", &system->k, &system->n, &system->m) <= 0) {
        system->has_solution = -3;
        return 1;
    }

    k = system->k;
    n = system->n;
    m = system->m;

    // читаем вектор коэфициентов
    linalg_new_vector(&system->C, n + m);
    linalg_fread_vector(file, system->C, n + m);

    // читаем матрицу A и вектор B
    linalg_new_matrix(&system->A, k, n + m + 1);
    linalg_fread_matrix(file, system->A, k, n + m + 1);

    // читаем B из прочитанного уже
    linalg_new_vector(&system->B, k);
    linalg_new_vector(&system->type, k);
    for (i = 0; i < k; i += 1) {
        system->B[i] = system->A[i][n + m];
        system->type[i] = 1;
    }

    system->has_solution = 0;

    return 0;
}

int mixprog_fwrite (FILE *file, mixprog_system *system) {
    int         i;
    int         j;

    // матрица ограничений
    fprintf(file, "Ограничения:\n");
    for (i = 0; i < system->k; i += 1) {
        for (j = 0; j < system->n + system->m; j += 1) {
            fprintf(file, "%6.2f ", system->A[i][j]);
        }
        // чтобы не создавать специально вектор int[]
        // мы содали вектор float[] для указания типа, а т.к. 
        // флоты в свитч писать нельзя, то делаем такую хитрую конструкцию
        switch ((int) system->type[i]) {
            case -1:
                fprintf(file, ">= ");
                break;
            case 0:
                fprintf(file, " = ");
                break;
            case 1:
                fprintf(file, "<= ");
                break;
            default:
                break;
        }
        fprintf(file, "%6.2f", system->B[i]);
        fprintf(file, "\n");
    }
    fprintf(file, "\n");

    if (system->has_solution == 1) {
        mixprog_fwrite_solution(file, system);
    } else {
        switch (system->has_solution) {
            case -1:
                fprintf(file, "Решения нет.\nСистема не ограничена.\n");
                break;
            case -2:
                fprintf(file, "Решения нет.\nОграничения не совместны.\n");
                break;
            default:
                // сюда по-идее мы не должны попасть
                break;
        }
    }

    fprintf(file, "\n");

    return 0;
}

int mixprog_fwrite_solution (FILE *file, mixprog_system *system) {
    fprintf(file, "X = ");
    linalg_fwrite_vector(file, system->solution_x, system->n);

    fprintf(file, "Y = ");
    linalg_fwrite_vector(file, system->solution_y, system->m);

    fprintf(file, "Оценка(решение) = %5.2f\n", system->solution_f);

    return 0;
}

int mixprog_fwrite_line (FILE *file) {
    int         i;

    for (i = 0; i < 80; i += 1) {
        fprintf(file, "=");
    }
    fprintf(file, "\n");
    
    return 0;
}

int mixprog_new (mixprog_system **system, int k, int n, int m) {
    (*system) = (mixprog_system *) malloc(sizeof(mixprog_system));

    linalg_new_matrix(&(*system)->A, k, n + m + 1);
    linalg_new_vector(&(*system)->C, n + m);
    linalg_new_vector(&(*system)->B, k);
    linalg_new_vector(&(*system)->type, k);

    (*system)->has_solution = 0;
    (*system)->k = k;
    (*system)->n = n;
    (*system)->m = m;
    GLOBAL_ID += 1;
    (*system)->id = GLOBAL_ID;

    return 0;
}

// копирование без учета размерова rsv, предполагается, что 
// под него уже выделена память и заданы размеры не меньше,
// чем размеры src
int mixprog_copy (mixprog_system *src, mixprog_system *rsv) {
    int         i;
    int         j;

    for (i = 0; i < src->k; i += 1) {
        for (j = 0; j < src->n + src->m; j += 1) {
            rsv->A[i][j] = src->A[i][j];
        }

        rsv->B[i] = src->B[i];
        rsv->type[i] = src->type[i];
    }

    for (j = 0; j < src->n + src->m; j += 1) {
        rsv->C[j] = src->C[j];
    }

    return 0;
}

int mixprog_free (mixprog_system **system) {
    if ((*system)->has_solution > -3) {
        linalg_del_vector(&(*system)->C);
        linalg_del_vector(&(*system)->B);
        linalg_del_vector(&(*system)->type);

        linalg_del_matrix(&(*system)->A, (*system)->k);

        if ((*system)->has_solution == 1) {
            (*system)->solution_y = NULL;
            linalg_del_vector(&(*system)->solution_x);
        }
    }

    free(*system);

    *system = NULL;

    return 0;
}
