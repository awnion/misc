// Это модифицированный симплекс метод, который работает с любым типом 
// ограницений.
//
// Описание алгоритма взято здесь:  
// http://www.mathelp.spb.ru/applet/SimplexTool.htm
// За что авторам большое спасибо.
//
// TODO: решить проблему погрешности вычислений
#include "simplex.h"
#include "error.h"

// =============================================================================
// = simplex_create_table                                                      =
// =============================================================================
// Переводим постановку задачи ЛП в вид:
//               n               m+m         
//      ┌─────────────────┬──────────────┬───┐
//      │                 │              │   │
//      │                 │              │   │
//      │                 │              │   │
//    m │        A        │       D      │ B │
//      │                 │              │   │
//      │                 │              │   │
//      │                 │              │   │
//      ├─────────────────┼──────────────┼───┤
//      │       -С        │       0      │ 0 │
//      ├─────────────────┴──────────────┼───┤
//      │              оценка            │ 0 │
//      └────────────────────────────────┴───┘
//
// 
int simplex_create_table (simplex_system *system, matrix *table, int *basis) {
    int         i;
    int         j;
    
    int         n                   = system->n;
    int         m                   = system->m;

    linalg_new_matrix(table, m + 2, n + m + m + 1);    
    
    for (i = 0; i < m; i += 1) {
        (*table)[i][n + i] = system->type[i];

        if (system->type[i] == 1) {
            // дополнительный базис
            basis[i] = n + i;

            // Вставляем матрицу A
            for (j = 0; j < n; j += 1) {
                (*table)[i][j] = system->A[i][j];
            }
        } else {
            // искусственный базис
            basis[i] = n + m + i;

            (*table)[i][n + m + i] = 1;
            // Сичтаем оценку
            (*table)[m + 1][n + m + i] -= 1;

            // Сичтаем оценку
            (*table)[m + 1][n + i] -= system->type[i];

            // Вставляем матрицу A
            for (j = 0; j < n; j += 1) {
                (*table)[i][j] = system->A[i][j];
                // Сичтаем оценку
                (*table)[m + 1][j] -= system->A[i][j];
            }
        }
    }
    
    // Вставляем вектор -C
    for (i = 0; i < n; i += 1) {
        (*table)[m][i] = - system->C[i];
    }
    
    // Вставляем вектор B
    for (i = 0; i < m; i += 1) {
        (*table)[i][n + m + m] = system->B[i];
    }

    return 0;
}

// =============================================================================
// = simplex_solve                                                             =
// =============================================================================
// Решает задачу ЛП, заданную в форме:
// 
// C*X -> max
// A[M,N]*X[N] <= B[M]
// A[K,N]*X[N] == B[K]
// X >= 0
// 
int simplex_solve (simplex_system *system) {
    int         i;
    int         j;
    int         j0;
    int         i0;
    
    int         artificial_vars_in_basis    = 0;
    int         n                           = system->n;
    int         m                           = system->m;
    
    float       tmp;
    matrix      table;
    int			*basis;			            // базис
    
    // приводим к виду, где B >= 0
    for (i = 0; i < m; i += 1) {
        if (system->B[i] < 0) {
            system->B[i] *= -1;
            system->type[i] *= -1;
            linalg_invert_vector(system->A[i], n);
        }
    }

    basis = (int *) calloc(m ,sizeof(int));

    // строим таблицу
    simplex_create_table(system, &table, basis);

    // считаем количество "плохих" ограничений
    for (i = 0; i < m; i += 1) {
        if (system->type[i] < 1) {
            artificial_vars_in_basis += 1;
        }
    }

    for (;;) {  
        // дети, никогда так не делайте!
        // выход из цикла гарантируется только благодаря
        // правилу Бленда

        // FIXME: for debug
        //linalg_fwrite_matrix(stdout, table, m + 2, n + m + m + 1);
        //printf("=================================================================\n");

        // ищем рабочий столбец
        j0 = 0;
        if (artificial_vars_in_basis > 0) {
            // Фаза 1 (работаем с оценками)
            for (j = 1; j < n + m + m; j += 1) {
                if (table[m + 1][j] < 0 && table[m + 1][j0] > table[m + 1][j]) {
                    j0 = j;
                }
            }

            // тут тонкий момент
            // если мы не смогли избавиться от искусственных переменных
            // но они все равны "нулю", то мы ещё можем найти решение, 
            // если продолжим фазу 2
            if (table[m + 1][j0] >= 0) {
                for (i = 0; i < m; i += 1) {
                    if (basis[i] >= n + m && table[i][n + m + m] != 0) {
                        system->has_solution = -2;
                    }
                }

                if (system->has_solution == -2) {
                    break; // решения нет, не можем избавиться от искусственных переменных
                } else {
                    artificial_vars_in_basis = 0; // продолжаем работу
                }
            }
        } else {
            // Фаза 2 (не используем оценки)
            for (j = 1; j < n + m; j += 1) {
                if (table[m][j] < 0 && table[m][j0] > table[m][j]) {
                    j0 = j;
                }
            }
            if (table[m][j0] >= 0) {
                system->has_solution = 1;
                break; // нашли решение
            }
        }
        
        // ищем минимальное строго положительное отношение (B[i] / A[i][j])
        tmp = INFTY;
        i0 = 0;
        for (i = 0; i < m; i += 1) {
            if (table[i][j0] > 0 && table[i][n + m + m] / table[i][j0] < tmp) {
                tmp = table[i][n + m + m] / table[i][j0];
                i0 = i;
            }
        }

        // если не нашли ни одного, то решения нет
        if (tmp == INFTY) {
            system->has_solution = -1;
            break; // нет решения, система не ограничена
        }

        // i0 - выводим из базиса, j0 - добавляем и проверяем, что добавили:
        // т.е. ведем подсчет искусственных переменных в базисе
        if (basis[i0] >= n+m) {
            artificial_vars_in_basis -= 1;
        }
        if (j0 >= n+m) {
            artificial_vars_in_basis += 1;
        }
        basis[i0] = j0;

        // [i0, j0] - ведущий эл-т в Гауссе:
        for (i = 0; i < m + 2; i += 1) {
            if (i != i0) {
                tmp = table[i][j0];
                for (j = 0; j < n + m + m + 1; j += 1) {
                    table[i][j] -= table[i0][j] * tmp / table[i0][j0];
                }
            }
        }

        // нормализуем строку
        tmp = table[i0][j0];
        for (j = 0; j < n + m + m + 1; j += 1) {
            table[i0][j] /= tmp;
        }
    }

    if (system->has_solution == 1) {
        linalg_new_vector(&system->solution_x, n);

        // записываем вектор решения
        for (i = 0; i < m; i += 1) {
            if (basis[i] < n) {
                system->solution_x[basis[i]] = table[i][n + m + m];
            }
        }
        // записываем значение функции
        system->solution_f = table[m][n + m + m];
    }

    free(basis);
    linalg_del_matrix(&table, m + 2);
    
    return 0;
}

// =============================================================================
// = simplex_test                                                              =
// =============================================================================
// Проверяет работу симплекс метод
// 
// Вход: имена 2-х файлов input_filename и output_filename
// Формат input_filename:
// n m
// c_1 c_2 ... c_n
// a_1_1 a_1_2 ... a_1_n b_1 type_1 // ограничения типа
// a_2_1 a_2_2 ... a_2_n b_2 type_2
// ...
// a_m_1 a_m_2 ... a_m_n b_m type_m
// 
int simplex_test (char *input_filename, char *output_filename) {
    int					i;
    int					n;
    int					m;
    int					k;
    FILE				*input;
    FILE				*output;
    simplex_system		system;

    input = fopen(input_filename, "r");
    output = fopen(output_filename, "w");

    if (input != NULL && output != NULL) {
        fscanf(input, "%d %d", &n, &m);

        system.m = m;
        system.n = n;

        // читаем вектор коэфициентов
        linalg_new_vector(&system.C, n);
        linalg_fread_vector(input, system.C, n);

        // читаем матрицу A и вектор B
        linalg_new_matrix(&system.A, m, n + 2);
        linalg_fread_matrix(input, system.A, m, n + 2);

        // читаем B из прочитанного уже
        linalg_new_vector(&system.B, m);
        linalg_new_vector(&system.type, m);
        for (i = 0; i < m; i += 1) {
            system.B[i] = system.A[i][n];
            system.type[i] = system.A[i][n + 1];
        }

        // DO IT :)
        simplex_solve(&system);

        // выводим результат
        if (system.has_solution == 1) {
            linalg_fwrite_vector(output, system.solution_x, n);
            fprintf(output, "%5.2f\n", system.solution_f);
        } else {
            fprintf(output, "NO SOLUTION");
        }

        // собираем мусор
        linalg_del_matrix(&system.A, m);
        linalg_del_vector(&system.B);
        linalg_del_vector(&system.C);
        linalg_del_vector(&system.type);
        if (system.has_solution) {
            linalg_del_vector(&system.solution_x);
        }
        
        fclose(input);
        fclose(output);
    } else {
        if (input == NULL) {
            error_file_not_exists(input_filename);
        }
        if (output == NULL) {
            error_file_not_exists(output_filename);
        }
    }
    return 0;
}
