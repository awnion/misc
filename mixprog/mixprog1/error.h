#ifndef _ERROR_H_
#define _ERROR_H_

#include <stdio.h>

int error_report (const char message[]);
int error_file_not_exists (const char filename[]);

#endif /* _ERROR_H_ */
